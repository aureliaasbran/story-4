from django.shortcuts import render


def homepage(request):
    return render(request, 'main/homepage.html')

def educationpage(request) :
    return render(request, 'main/educationpage.html')

def familypage(request) :
    return render(request, 'main/familypage.html')

def interestspage(request) :
    return render(request, 'main/interestspage.html')

def contactpage(request) :
    return render(request, 'main/contactpage.html')
